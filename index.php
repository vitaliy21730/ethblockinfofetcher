<?php

/* May exists other implementations. For example pure eth node with json rpc access */

interface EthNodeRepository
{
    public function blockNumber(): int;

    public function getBlockByNumber(int $blockNumber, bool $showTransactions): array;
}

class EtherscanEthNodeRepository implements EthNodeRepository
{
    /**
     * @var string
     */
    private string $apiKey;

    /**
     * EtherscanEthNodeRepository constructor.
     * @param string $apiKey
     */
    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    private function requestByParams(string $action, array $params = [])
    {
        $params = array_merge([
            'module' => 'proxy',
            'action' => $action,
            'apikey' => $this->apiKey,
        ], $params);

        $query = http_build_query($params);

        $url = "https://api.etherscan.io/api?" . $query;

        $response = file_get_contents($url);

        if ($response === false) {
            throw new RuntimeException("Invalid etherscan response for url " . $url);
        }

        $decodedResponse = json_decode($response, true);

        if (!array_key_exists('id', $decodedResponse)) {
            throw new RuntimeException("Etherscan request processing error " . $url . " content" . $response);
        }

        if (array_key_exists('error', $decodedResponse)) {
            throw new RuntimeException("Node error " . $url . " content" . $response);
        }

        return $decodedResponse['result'];
    }

    public function blockNumber(): int
    {
        $response = $this->requestByParams('eth_blockNumber');
        return hexdec($response);
    }

    public function getBlockByNumber(int $blockNumber, bool $showTransactions): array
    {
        $response = $this->requestByParams('eth_getBlockByNumber', [
            'tag' => dechex($blockNumber),
            'boolean' => $showTransactions
        ]);

        return $response;
    }
}

interface Logger
{
    public function log(string $str, array $params = []): void;
}

/* Implements common logic independent from output type */

abstract class AbstractLogger implements Logger
{

    public function log(string $str, array $params = []): void
    {
        $strToLog = '';

        $strToLog .= date("c", time());

        $strToLog .= ' | ';

        $strToLog .= $str;

        if (count($params)) {
            $strToLog .= ' ' . json_encode($params);
        }

        $this->writeLogStr($strToLog);
    }

    abstract protected function writeLogStr(string $str);
}

class StdOutLogger extends AbstractLogger
{
    protected function writeLogStr(string $str)
    {
        echo $str . PHP_EOL;
    }
}

class BlockInfo
{
    public int $blockNumber;
    public int $minGasPrice;
    public int $maxGasPrice;
    public int $avgGasPrice;

    /**
     * BlockInfo constructor.
     * @param int $blockNumber
     * @param int $minGasPrice
     * @param int $maxGasPrice
     * @param int $avgGasPrice
     */
    public function __construct(int $blockNumber, int $minGasPrice, int $maxGasPrice, int $avgGasPrice)
    {
        $this->blockNumber = $blockNumber;
        $this->minGasPrice = $minGasPrice;
        $this->maxGasPrice = $maxGasPrice;
        $this->avgGasPrice = $avgGasPrice;
    }
}

interface RowMapper
{
    public function fromRow(array $row);
}

class BlockInfoRowMapper implements RowMapper
{

    public function fromRow(array $row)
    {
        return new BlockInfo(
            $row['block_number'],
            $row['min_gas_price'],
            $row['max_gas_price'],
            $row['avg_gas_price'],
        );
    }
}

interface BlockInfoRepository
{
    public function getLast(): ?BlockInfo;

    public function insert(BlockInfo $blockInfo);

    public function removeOld(int $blocksCount);
}

class BlockInfoPdoRepository implements BlockInfoRepository
{
    /**
     * @var PDO
     */
    private PDO $pdo;
    /**
     * @var RowMapper
     */
    private RowMapper $rowMapper;
    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * BlockInfoPdoRepository constructor.
     * @param PDO $pdo
     * @param RowMapper $rowMapper
     * @param Logger $logger
     */
    public function __construct(PDO $pdo, RowMapper $rowMapper, Logger $logger)
    {
        $this->pdo = $pdo;
        $this->rowMapper = $rowMapper;
        $this->logger = $logger;
    }

    public function getLast(): ?BlockInfo
    {
        $sql = 'select * from blocks_info order by block_number desc limit 1';

        $rez = $this->pdo->query($sql);

        if ($rez->rowCount() == 0) {
            return null;
        }

        foreach ($rez as $row) {
            return $this->rowMapper->fromRow($row);
        }
    }

    public function insert(BlockInfo $blockInfo)
    {
        $sql = "INSERT INTO blocks_info (block_number, min_gas_price, max_gas_price, avg_gas_price) values (?, ?, ?, ?)";
        $statement = $this->pdo->prepare($sql);

        $statement->execute([
            $blockInfo->blockNumber,
            $blockInfo->minGasPrice,
            $blockInfo->maxGasPrice,
            $blockInfo->avgGasPrice,
        ]);

        if ($statement->rowCount() == 1) {
            $this->logger->log("Saved block info", [$blockInfo]);
        } else {
            throw new RuntimeException("Processed 0 rows when block info inserting " . $sql);
        }
    }

    public function removeOld(int $blocksCount)
    {
        $sql = sprintf("select block_number from blocks_info order by block_number desc limit %d, 1", $blocksCount);
        $rez = $this->pdo->query($sql);

        if ($rez->rowCount() != 1) {
            $this->logger->log("Finding block number row count 0. Skipping");
            return;
        }

        $blockNumber = null;
        foreach ($rez as $row) {
            $blockNumber = $row['block_number'];
        }

        $this->logger->log("Removing blocks with number <= $blockNumber");

        $sql = "DELETE FROM blocks_info where block_number <= ?";
        $statement = $this->pdo->prepare($sql);
        $statement->execute([$blockNumber]);

        $this->logger->log("Removed old " . $statement->rowCount());
    }
}

class BlockInfoProcessor
{

    /**
     * @var EthNodeRepository
     */
    private EthNodeRepository $ethNodeRepository;

    /**
     * @var BlockInfoRepository
     */
    private BlockInfoRepository $blockInfoRepository;

    /**
     * @var Logger
     */
    private Logger $logger;

    /**
     * BlockInfoProcessor constructor.
     * @param EthNodeRepository $ethNodeRepository
     * @param BlockInfoRepository $blockInfoRepository
     * @param Logger $logger
     */
    public function __construct(
        EthNodeRepository $ethNodeRepository,
        BlockInfoRepository $blockInfoRepository,
        Logger $logger)
    {
        $this->ethNodeRepository = $ethNodeRepository;
        $this->blockInfoRepository = $blockInfoRepository;
        $this->logger = $logger;
    }

    public function parse(int $blockInfoCount, int $transactionCount)
    {
        $currentBlockNumber = $this->ethNodeRepository->blockNumber();

        $lastBlockInfo = $this->blockInfoRepository->getLast();

        if ($lastBlockInfo == null) {
            $lastProcessedBlockNumber = 0;
        } else {
            $lastProcessedBlockNumber = $lastBlockInfo->blockNumber;
        }

        $diff = $currentBlockNumber - $lastProcessedBlockNumber;

        if ($diff == 0) {
            $this->logger->log("Difference is 0. No need processing. Returns.");
            return;
        } else if ($diff < $blockInfoCount) {
            $blocksCountToFetch = $diff - 1;
        } else {
            $blocksCountToFetch = $blockInfoCount;
        }

        $lowerBlockNumberToFetch = $currentBlockNumber - $blocksCountToFetch;

        $this->logger->log("Fetching blocks", [
            'currentBlockNumber' => $currentBlockNumber,
            'lastProcessedBlockNumber' => $lastProcessedBlockNumber,
            'diff' => $diff,
            'blocksCountToFetch' => $blocksCountToFetch,
            'lowerBlockNumberToFetch' => $lowerBlockNumberToFetch
        ]);

        for ($i = $currentBlockNumber; $i >= $lowerBlockNumberToFetch; $i--) {
            $this->logger->log('Start processing block ' . $i);
            $blockInfo = $this->ethNodeRepository->getBlockByNumber($i, true);
            $transactions = $blockInfo['transactions'];

            $transactionsCount = count($transactions);
            if ($transactionsCount == 0) {
                $this->logger->log('Transactions count 0. Returns.');
                continue;
            } else {
                $this->logger->log('Transactions count ' . count($transactions));
            }

            $transactions = array_slice($transactions, 0, $transactionCount);

            $minGasPrice = PHP_INT_MAX;
            $maxGasPrice = 0;
            $sumGasPrice = 0;

            foreach ($transactions as $transaction) {
                $gasPrice = hexdec($transaction['gasPrice']);

                if ($minGasPrice > $gasPrice) {
                    $minGasPrice = $gasPrice;
                }

                if ($maxGasPrice < $gasPrice) {
                    $maxGasPrice = $gasPrice;
                }

                $sumGasPrice += $gasPrice;
            }

            $avgGasPrice = $sumGasPrice / count($transactions);

            $blockInfo = new BlockInfo($i, $minGasPrice, $maxGasPrice, (int)$avgGasPrice);

            $this->blockInfoRepository->insert($blockInfo);
        }
    }

    public function clean(int $blocksCount)
    {
        $this->blockInfoRepository->removeOld($blocksCount);
    }

}

/* .env emulating */
const MYSQL_CONNECTION_HOST = 'mysql';
const MYSQL_CONNECTION_PORT = 3306;
const MYSQL_CONNECTION_USER = 'root';
const MYSQL_CONNECTION_PASSWORD = 'secret';
const MYSQL_CONNECTION_DB_NAME = 'test';

const ETHERSCAN_API_KEY = "6B7TM7WZEP12EAVDICSWUHKWH5DI9GGD4U";

const BLOCK_INFO_COUNT = 20;
const BLOCK_INFO_FIRST_TRANSACTIONS = 20;

/* PDO Connection */
$connectionString = sprintf(
    "mysql:host=%s;port=%s;dbname=%s",
    MYSQL_CONNECTION_HOST, MYSQL_CONNECTION_PORT, MYSQL_CONNECTION_DB_NAME
);

$pdo = new PDO($connectionString, MYSQL_CONNECTION_USER, MYSQL_CONNECTION_PASSWORD);

/* Setting err mode to exceptions */
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/* DI emulating */
$logger = new StdOutLogger();
$etherscanEthNodeRepository = new EtherscanEthNodeRepository(ETHERSCAN_API_KEY);
$blockInfoRowMapper = new BlockInfoRowMapper();
$blockInfoRepository = new BlockInfoPdoRepository($pdo, $blockInfoRowMapper, $logger);
$blockInfoProcessor = new BlockInfoProcessor($etherscanEthNodeRepository, $blockInfoRepository, $logger);

/* App logic */
$blockInfoProcessor->parse(BLOCK_INFO_COUNT, BLOCK_INFO_FIRST_TRANSACTIONS);
$blockInfoProcessor->clean(BLOCK_INFO_COUNT);
