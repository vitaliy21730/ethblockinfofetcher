/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 15/07/2020 15:39:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blocks_info
-- ----------------------------
DROP TABLE IF EXISTS `blocks_info`;
CREATE TABLE `blocks_info`  (
  `block_number` bigint UNSIGNED NOT NULL,
  `min_gas_price` decimal(18, 0) NOT NULL,
  `max_gas_price` decimal(18, 0) NOT NULL,
  `avg_gas_price` decimal(18, 0) NOT NULL,
  PRIMARY KEY (`block_number`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
